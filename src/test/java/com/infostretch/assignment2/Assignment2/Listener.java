package com.infostretch.assignment2.Assignment2;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class Listener implements ITestListener, ISuiteListener, IInvokedMethodListener {

	public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
		System.out.println(" before Invocation of method "+method.getTestMethod().getMethodName());
	}

	public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
		System.out.println(" after Invocation of method "+method.getTestMethod().getMethodName());		
	}

	public void onStart(ISuite suite) {
		System.out.println(" onStart "+suite.getName());	
	}

	public void onFinish(ISuite suite) {
		System.out.println(" onFinish "+suite.getName());	
	}

	public void onTestStart(ITestResult result) {
		System.out.println(" onTestStart of "+result.getName());
	}

	public void onTestSuccess(ITestResult result) {
		System.out.println(" onTestSuccess of "+result.getName());
	}

	public void onTestFailure(ITestResult result) {
		System.out.println(" onTestFailure of "+result.getName());
	}

	public void onTestSkipped(ITestResult result) {
		System.out.println(" onTestSkipped of "+result.getName());
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println(" onTestFailedButWithinSuccessPercentage "+result.getStatus());
	}

	public void onStart(ITestContext context) {
		System.out.println(" onStart "+context.getName());
	}

	public void onFinish(ITestContext context) {
		System.out.println(" onFinish "+context.getName());
	}

}
