package com.infostretch.assignment2.Assignment2;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Listeners(com.infostretch.assignment2.Assignment2.Listener.class)
public class Main {

	private WebDriver driver;
	private WebDriverWait wait;
	private final int DELAY_SECONDS = 10;
	private final String CHROME_DRIVER_KEY = "webdriver.chrome.driver";
	private final String CHROME_DRIVER_PATH = "D:\\Eclipse\\Webdrivers\\chromedriver.exe";
	private final String FIREFOX_DRIVER_KEY = "webdriver.gecko.driver";
	private final String FIREFOX_DRIVER_PATH = "D:\\Eclipse\\Webdrivers\\geckodriver.exe";

	@BeforeTest
	@Parameters("browser")
	public void setup(String browser) {
		if (browser.equalsIgnoreCase("chrome")) {
			File file = new File(CHROME_DRIVER_PATH);
			System.setProperty(CHROME_DRIVER_KEY, file.getAbsolutePath());
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("firefox")) {
			File file = new File(FIREFOX_DRIVER_PATH);
			System.setProperty(FIREFOX_DRIVER_KEY, file.getAbsolutePath());
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.navigate().to("https://www.kayak.co.in/flights");
		wait = new WebDriverWait(driver, DELAY_SECONDS);
	}

	@Test(dataProvider = "data-provider")
	public void verifyflightsearch(String origin, String destination) {
		enterOrigin(origin);
		enterDestination(destination);
		enterDates(tomorrow(), dayAfterTomorrow());
		searchResults();
	}

	@AfterTest
	public void tearDown() {
		 driver.quit();
	}

	@DataProvider(name = "data-provider")
	public Object[][] DataProvider() {
		return new Object[][] { { "Mumbai", "Krabi" } };
	}

	public void enterOrigin(String origin_value) {
		driver.findElement(By.cssSelector(".primary-content [id*='origin-airport-display-inner']")).click();
		WebElement origin = driver.findElement(By.name("origin"));
		wait.until(ExpectedConditions.visibilityOf(origin));
		origin.click();
		origin.clear();
		origin.sendKeys(origin_value);
		wait.until(ExpectedConditions.presenceOfElementLocated(
				By.xpath("//*[contains(@id,'origin-airport-smartbox-dropdown')] //*[contains(@data-short-name,'"
						+ origin_value + "')]")));
		WebElement originDropdown = driver
				.findElement(By.xpath("//*[contains(@id,'origin-airport-smartbox-dropdown')]"));
		List<WebElement> list = originDropdown.findElements(By.tagName("li"));
		java.util.Iterator<WebElement> originItems = list.iterator();
		while (originItems.hasNext()) {
			WebElement row = originItems.next();
			String name = row.getAttribute("data-short-name");
			System.out.println("Checking for:" + name);
			if (name != null && name.contains(origin_value)) {
				System.out.println(row.getAttribute("data-short-name"));
				row.click();
				break;
			}
		}
	}

	public void enterDestination(String destinaiton_value) {
		wait.withTimeout(Duration.ofSeconds(DELAY_SECONDS));
		WebElement destination = driver.findElement(By.name("destination"));
		wait.until(ExpectedConditions.visibilityOf(destination));
		destination.click();
		destination.clear();
		destination.sendKeys(destinaiton_value);
		wait.until(ExpectedConditions.presenceOfElementLocated(
				By.xpath("//*[contains(@id,'destination-airport-smartbox-dropdown')] //*[contains(@data-short-name,'"
						+ destinaiton_value + "')]")));
		WebElement destinationDropdown = driver
				.findElement(By.xpath("//*[contains(@id,'destination-airport-smartbox-dropdown')]"));
		List<WebElement> list = destinationDropdown.findElements(By.tagName("li"));
		java.util.Iterator<WebElement> destinationItems = list.iterator();
		while (destinationItems.hasNext()) {
			WebElement row = destinationItems.next();
			String name = row.getAttribute("data-short-name");
			System.out.println("Checking for :" + name);
			if (name != null && name.contains(destinaiton_value)) {
				System.out.println(row.getAttribute("data-short-name"));
				row.click();
				break;
			}
		}
	}

	public void enterDates(String depart, String _return) {
		wait.withTimeout(Duration.ofSeconds(DELAY_SECONDS));
		WebElement departure_date = driver.findElement(By.xpath("//*[contains(@id,'dateRangeInput-display-start')]"));
		wait.until(ExpectedConditions.elementToBeClickable(departure_date));
		departure_date.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@id,'cal-monthsGrid')]")));
		WebElement element = driver.findElement(By.xpath("//*[contains(@id,'cal-monthsGrid')]"));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.findElement(By.xpath("//*[@aria-label='" + depart + "']")).click();
		element.findElement(By.xpath("//*[@aria-label='" + _return + "']")).click();
	}

	public void searchResults() {
		wait.withTimeout(Duration.ofSeconds(DELAY_SECONDS));
		WebElement search = driver.findElement(By.xpath("//*[contains(@id,'submit')]"));
		search.click();
	}

	
	public String tomorrow() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 1);
		return new SimpleDateFormat("MMMMM dd").format(c.getTime());
	}

	public String dayAfterTomorrow() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 2);
		return new SimpleDateFormat("MMMMM dd").format(c.getTime());
	}

	public void delay(long ms) {
		try {
			Thread.sleep(ms);
		} catch (Exception e) {
		}
	}

}
